package com.example.todo.model

import android.os.Parcel
import android.os.Parcelable

class Task(
    val id: Long?,
    val description: String,
    var isDone: Boolean,
    var isUrgent: Boolean,
) {
    companion object {

    }

    override fun toString(): String {
        return "Task(id=$id, description='$description', isDone=$isDone, isUrgent=$isUrgent)"
    }
}

