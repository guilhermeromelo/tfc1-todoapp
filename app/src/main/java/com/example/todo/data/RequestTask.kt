package com.example.todo.data

import VolleyCallBack
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.example.todo.model.Task
import org.json.JSONArray
import java.nio.charset.Charset
import org.json.JSONObject

class RequestTask(context: Context) {
    private val queue = Volley.newRequestQueue(context)

    companion object {
        private const val APIurl = "http://10.0.2.2:8100"
    }

    fun startRequestingTasks() {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object : Runnable {
            override fun run() {
                processRequest(null)
                handler.postDelayed(this, 5000)
            }
        })
    }
    private fun processRequest(c: VolleyCallBack?) {
        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET,
            "${APIurl}/tasks",
            null,
            // Response.Listener
            { response ->
                val tasks = this.JSONArrayToTasks(response)
                TasksSingleton.updateTasks(tasks)
                c?.onSuccess();
            },
            // Response.ErrorListener
            { volleyError ->
                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
            }
        )
        this.queue.add(jsonArrayRequest)
    }
    fun JSONArrayToTasks(jsonArray: JSONArray): ArrayList<Task> {
        val tasks = ArrayList<Task>()
        for (i in 0..(jsonArray.length()-1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val id = jsonObj.getLong("id")
            val description = jsonObj.getString("description")
            val isDone = jsonObj.getInt("isDone") == 1
            val isUrgent = jsonObj.getInt("isUrgent") == 1
            tasks.add( Task(id, description, isDone, isUrgent)
            )
        }

        return tasks
    }

    fun addNewTask(task: Task, c: VolleyCallBack) {
        Log.d("RequestStatus", "Iniciando")
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.POST,
            "${APIurl}/tasks/new",
            this.taskToJSONObject(task),
            // Response.Listener
            { response ->
                Log.d("RequestResponse", response.toString())
                processRequest(c)
            },
            // Response.ErrorListener
            { volleyError ->
                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
            }
        )

        this.queue.add(jsonArrayRequest);
    }

    fun taskToJSONObject(task: Task): JSONObject {
        val jsonObject = JSONObject();
        if(task.id != null) jsonObject.put("id",task.id);
        if(task.description != null) jsonObject.put("description",task.description);
        if(task.isDone != null) jsonObject.put("isDone",task.isDone);
        if(task.isUrgent != null) jsonObject.put("isUrgent",task.isUrgent);
        return jsonObject;
    }

//UPDATE WITH PATCH METHOD
//    fun updateTask(task: Task) {
//        Log.d("RequestStatus", "IniciandoUpdate")
//        val jsonArrayRequest = JsonObjectRequest(
//            Request.Method.PATCH,
//            "${APIurl}/tasks",
//            this.taskToJSONObject(task),
//            // Response.Listener
//            { response ->
//                Log.d("RequestResponse", response.toString())
//                processRequest(null)
//            },
//            // Response.ErrorListener
//            { volleyError ->
//                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
//            }
//        )
//
//        this.queue.add(jsonArrayRequest);
//    }

    fun updateTask(task: Task) {
        Log.d("RequestStatus", "IniciandoUpdate")
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.PUT,
            "${APIurl}/tasks/done/${task.isDone}/${task.id}",
            this.taskToJSONObject(task),
            // Response.Listener
            { response ->
                Log.d("RequestResponse", response.toString())
                processRequest(null)
            },
            // Response.ErrorListener
            { volleyError ->
                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
            }
        )

        this.queue.add(jsonArrayRequest);
    }

    fun deleteTask(task: Task) {
        Log.d("RequestStatus", "Deletando")
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.DELETE,
            "${APIurl}/tasks/del/${task.id}",
            JSONObject(),
            // Response.Listener
            { response ->
                Log.d("RequestResponse", response.toString())
                processRequest(null)
            },
            // Response.ErrorListener
            { volleyError ->
                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
            }
        )

        this.queue.add(jsonArrayRequest);
    }
}