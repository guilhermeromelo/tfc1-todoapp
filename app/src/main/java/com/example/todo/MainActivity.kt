package com.example.todo

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowInsetsController
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.toArgb
import br.pro.viniciusfm.funnymoviesapp.ui.components.frames.MainFrame
import com.example.todo.data.RequestTask
import com.example.todo.ui.theme.ToDoTheme

class MainActivity : AppCompatActivity() {

    private lateinit var requestTask: RequestTask
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestTask = RequestTask(this)
        this.requestTask.startRequestingTasks()
        setContent {
            ToDoTheme {
                this.SetupConfigsUiConfigs();
                MainFrame(this.requestTask, this)
            }
        }
    }

    @Composable
    private fun SetupConfigsUiConfigs() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (MaterialTheme.colors.isLight) {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS,
                        WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
                    )
            } else {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        0, WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
                    )
            }
        }
        window.statusBarColor = MaterialTheme.colors.primaryVariant.toArgb()
    }

}