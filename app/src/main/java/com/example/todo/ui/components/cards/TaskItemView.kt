package com.example.todo.ui.components

import AlertDialogCallbackInterface
import android.content.Context
import android.text.Html
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.todo.data.RequestTask
import com.example.todo.data.TasksSingleton
import com.example.todo.model.Task
import com.example.todo.ui.components.dialogs.AlertDialogRemoveTask
import com.example.todo.ui.theme.ToDoTheme
import kotlinx.coroutines.launch

@Composable
fun TaskItemView(task: Task, requestTask: RequestTask?, context: Context?) {
    var taskChecked by remember {
        mutableStateOf(false)
    }
    taskChecked = task.isDone;
    var showAlertDialog by remember {mutableStateOf(false)}
    var maxlines = remember { mutableStateOf(1)};

    Card(
        modifier =
        Modifier
            .fillMaxWidth()
            .padding(vertical = 2.5.dp, horizontal = 5.dp)
    ) {
        Row (
            modifier = Modifier
                .height(intrinsicSize = IntrinsicSize.Min)
                .clickable {
                    if (maxlines.value == 1) maxlines.value = 10000 else maxlines.value = 1;
                }){
            Box(
                modifier = Modifier
                    .width(15.dp)
                    .background(if (task.isUrgent) Color.Red else Color.Green)
                    .fillMaxHeight()
            );
            Checkbox(
                checked = taskChecked,
                modifier = Modifier
                    .width(40.dp)
                    .align(alignment = Alignment.CenterVertically),
                onCheckedChange = {
                    taskChecked = !taskChecked;
                    task.isDone = taskChecked;
                    requestTask?.updateTask(task);
                    Toast.makeText(context, Html.fromHtml("<font color='#FF0000' ><b>" + "The task was updated!" + "</b></font>"), Toast.LENGTH_LONG).show()
                });
            Text(
                task.description,
                maxLines = maxlines.value,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .padding(start = 5.dp, end = 10.dp)
                    .fillMaxWidth()
                    .weight(1f)
                    .align(alignment = Alignment.CenterVertically),
                );
            FloatingActionButton(
                modifier = Modifier
                    .width(40.dp)
                    .height(40.dp)
                    .align(alignment = Alignment.CenterVertically),
                onClick = {
                    showAlertDialog = true;
                }
            ) {
                Icon(Icons.Filled.Delete, "Apagar")
            }
            if(showAlertDialog){
                AlertDialogRemoveTask(task = task,
                    object : AlertDialogCallbackInterface {
                        override fun onUserResponse(delete: Boolean) {
                            if(delete && task.id != null){
                                TasksSingleton.removeTask(task);
                                requestTask?.deleteTask(task);
                                Toast.makeText(context, Html.fromHtml("<font color='#FF0000' ><b>" + "The task was deleted!" + "</b></font>"), Toast.LENGTH_LONG).show()
                            }
                            showAlertDialog = false;
                        }
                    }
                )
            }
        }

    }
}

@Preview
@Composable
fun previewTaskItemView() {
    var newTaskText = remember { mutableStateOf(TextFieldValue("")) }
    ToDoTheme {
        val task = Task(0,
            "Cozinhar o jantar, Cozinhar o jantar, Cozinhar o jantar, Cozinhar o jantar",
            false,
            false
        );
        TaskItemView(task = task,null,null);
    }
}