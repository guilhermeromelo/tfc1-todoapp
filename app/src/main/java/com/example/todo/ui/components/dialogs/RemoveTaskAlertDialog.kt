package com.example.todo.ui.components.dialogs
import AlertDialogCallbackInterface
import androidx.compose.material.*
import androidx.compose.runtime.*
import com.example.todo.model.Task

//https://foso.github.io/Jetpack-Compose-Playground/material/alertdialog/

@Composable
fun AlertDialogRemoveTask(task: Task, callback: AlertDialogCallbackInterface) {
    AlertDialog(
        title = {
            Text(text = "Delete task")
        },
        text = {
            Text("Are you sure you want to delete task \"${task.description}\" ? ")
        },
        confirmButton = {
            Button(
                onClick = { callback.onUserResponse(true) }) {
                Text("Delete")
            }
        },
        dismissButton = {
            Button(
                onClick = { callback.onUserResponse(false) }) {
                Text("Cancelar")
            }
        },
        onDismissRequest = {},
    )
}