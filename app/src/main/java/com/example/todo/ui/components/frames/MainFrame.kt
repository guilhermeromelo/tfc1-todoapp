package br.pro.viniciusfm.funnymoviesapp.ui.components.frames

import VolleyCallBack
import android.content.Context
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.todo.data.RequestTask
import com.example.todo.data.TasksSingleton
import com.example.todo.model.Task
import com.example.todo.ui.components.TaskItemView
import com.example.todo.ui.theme.ToDoTheme
import kotlinx.coroutines.launch

@Composable
fun MainFrame(requestTask: RequestTask, context: Context) {
    var newTaskText = remember { mutableStateOf(TextFieldValue("")) }
    var isUrgent = remember { mutableStateOf(false) }
    val listState = rememberLazyListState()
    val coroutineScope = rememberCoroutineScope()
    Scaffold(
        modifier =
        Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.background),
        bottomBar = {
            Column() {
                Row() {
                    Switch(
                        checked = isUrgent.value,
                        onCheckedChange = {
                            isUrgent.value = !isUrgent.value;
                        },
                    )
                    Text(
                        "Is Urgent ?",
                        modifier = Modifier.align(alignment = Alignment.CenterVertically),
                    )
                }
                Row() {
                    TextField(
                        value = newTaskText.value,
                        onValueChange = { newValue -> newTaskText.value = newValue },
                        placeholder = { Text("Type something to Add...") },
                    )
                    Button(onClick = {
                        Log.d("botão","click")
                        requestTask.addNewTask(Task(0,newTaskText.value.text,false, isUrgent.value),
                            object : VolleyCallBack {
                                override fun onSuccess() {
                                    coroutineScope.launch {
                                        listState.animateScrollToItem(TasksSingleton.getTasks().size)
                                    }
                                }
                            });
                        newTaskText.value = TextFieldValue("")

                    }, Modifier.width(80.dp)) {
                        Text("ADD")
                    }
                }
            }
        }
    ) { innerPadding ->
        Box(
            modifier = Modifier.padding(innerPadding)
        ) {
            Column() {
                LazyColumn (state = listState) {
                    items(TasksSingleton.getTasks()) { task ->
                        TaskItemView(task = task, requestTask = requestTask, context = context)
                    }
                }
            }
        }
    }
}